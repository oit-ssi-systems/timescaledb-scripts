#!/bin/bash

cd /srv/docker-compose/timescale && \
    docker-compose exec timescale pg_dump -U postgres -Fc -f /db_backups/perfdump.sql perfdump && \
    cp /tank/db_backups/perfdump.sql /srv/db_backups/
