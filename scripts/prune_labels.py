#!/usr/bin/env python3
import sys
import os
from promchemist.base import session, Metric, Label
from datetime import datetime, timedelta


def main():
    if 'TIMESCALEDB_CONNECT' not in os.environ:
        sys.stderr.write(
            ("Err.  Please set TIMESCALEDB_CONNECT to your postgresql "
             "connect string\n"))
        sys.exit(2)

    now = datetime.now()
    yesterday = now - timedelta(days=1)
    for label in session.query(Label).yield_per(10000):

        # Do a freshness check first, just for some quick ways to move on
        recent_metric_count = session.query(Metric)\
            .filter(Metric.labels_id == label.id)\
            .filter(Metric.time > yesterday)\
            .count()

        # If we have recent metrics, just move on
        if recent_metric_count > 0:
            continue

        full_metric_count = session.query(Metric)\
            .filter(Metric.labels_id == label.id)\
            .count()

        # If we have a record ever, just continue of course
        if full_metric_count > 0:
            continue

        # These should all be comin' up empty
        print(label.metric_name, label.labels, recent_metric_count)


#       session.commit()
#       print()
#       print("Deleted %s" % deleted)
#       print()

    return 0


if __name__ == "__main__":
    sys.exit(main())
