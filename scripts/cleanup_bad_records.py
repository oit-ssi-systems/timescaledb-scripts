#!/usr/bin/env python3
import sys
import hvac
import psycopg2
import os


def chunks(l, n):
    """Yield successive n-sized chunks from l."""
    for i in range(0, len(l), n):
        yield l[i:i + n]


def main():

    server = 'timescale-02.oit.duke.edu'
    vault = hvac.Client(url=os.environ['VAULT_ADDR'],
                        token=os.environ['VAULT_TOKEN'])

    creds = vault.read(
        'secret/linux/timescaledb/%s/creds/postgres' % server)['data']

    conn = psycopg2.connect(
        "host=%s user=%s password=%s sslmode=require dbname=perfdump" % (
            server, creds['username'], creds['password']))
    cur = conn.cursor()

    # First, blow away some old garbage
    fuzzy_matches = ['interrupts_CPU%']
    for fuzzy_match in fuzzy_matches:
        print("Deleteing fuzzy match: %s" % fuzzy_match)
        cur.execute("""
SELECT id
FROM metrics_labels
WHERE metric_name LIKE '%s'
""" % fuzzy_match)
        results = [item for item, in cur.fetchall()]

        batch = 1000
        chungus = chunks(results, batch)
        total_chungus = len(results) / batch
        i = 1
        for chunk in chungus:
            delete_ids = tuple(chunk)
            print(delete_ids)
            cur.execute("""
DELETE
FROM metrics_values
WHERE labels_id IN %s
  AND time > NOW() - interval '1 month'
""" % (delete_ids, ))
            conn.commit()
            print("%s    / %s" % (i, total_chungus))

            i = i + 1

    return 0


if __name__ == "__main__":
    sys.exit(main())
