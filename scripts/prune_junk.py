#!/usr/bin/env python3
import sys
import os
from promchemist.base import session, Metric, Label
from datetime import datetime, timedelta


def iter_times(increment='days'):
    now = datetime.now()
    earliest_time = now - timedelta(days=7)
    current_time = earliest_time
    time_ranges = []

    while current_time <= now:
        time = current_time
        if increment == 'days':
            start = datetime(time.year, time.month, time.day, 0, 0)
            end = datetime(time.year, time.month, time.day, 23, 59)
            time_ranges.append([start, end])
            current_time += timedelta(days=1)
        elif increment == 'hours':
            start = datetime(time.year, time.month, time.day, time.hour, 0)
            end = datetime(time.year, time.month, time.day, time.hour, 59)
            time_ranges.append([start, end])
            current_time += timedelta(hours=1)
        elif increment == 'minutes':
            start = datetime(time.year, time.month, time.day, time.hour,
                             time.minute, 0)
            end = datetime(time.year, time.month, time.day, time.hour,
                           time.minute, 59)
            time_ranges.append([start, end])
            current_time += timedelta(minutes=1)
    return time_ranges


def main():
    if 'TIMESCALEDB_CONNECT' not in os.environ:
        sys.stderr.write(
            ("Err.  Please set TIMESCALEDB_CONNECT to your postgresql "
             "connect string\n"))
        sys.exit(2)

#   deleted = 0
    for start, end in iter_times(increment='minutes'):
        print("----------------------------------")
        print(start, end)
        timer = datetime.now()
        labels_q = session.query(Metric.labels_id)\
            .filter(Metric.time.between(start, end))\
            .join(Label)\
            .yield_per(10000)\
            .filter(Label.metric_name.like('cpu_usage_%'))\
            .filter(Label.labels['cpu'].astext != 'cpu-total')

        ids = []
        for item in labels_q:
            ids.append(item[0])
        print("%s Colated %s ids" % ((datetime.now() - timer), len(ids)))

        for label_id in ids:
            q = session.query(Metric)\
                .yield_per(1000)\
                .filter(Metric.labels_id == label_id)
            print("%s Built delete query for %s" % (
                (datetime.now() - timer), label_id))
            q.delete(synchronize_session=False)
            print("%s Deleted record" % (datetime.now() - timer))

            session.commit()
            print("%s Committed records" % (datetime.now() - timer))

        print("Finished deleting %s records in %s %s -> %s" % (
            len(ids), (datetime.now() - timer), start, end))

        # old interrupts
        continue
        labels_q = session.query(Metric.labels_id)\
            .filter(Metric.time.between(start, end))\
            .join(Label)\
            .yield_per(1000)\
            .filter(Label.metric_name.like('interrupts_CPU%')).limit(50000)
        ids = []
        for item in labels_q:
            ids.append(item[0])

        if len(ids) == 0:
            print("%s No ids for %s -> %s"(
                (datetime.now() - timer), start, end))

        try:
            q = session.query(Metric)\
                .yield_per(1000)\
                .filter(Metric.labels_id.in_(ids[0:10000]))
            q.delete(synchronize_session=False)
            session.commit()
        except:
            sys.stderr.write("!! Error deleting !!\n")
            continue

        print("Finished deleting %s records in %s %s -> %s" % (
            len(ids), (datetime.now() - timer), start, end))

#       session.commit()
#       print()
#       print("Deleted %s" % deleted)
#       print()

    return 0


if __name__ == "__main__":
    sys.exit(main())
