#!/usr/bin/env python3
import sys
import os
from promchemist.base import session, Metric
from datetime import datetime, timedelta
from sqlalchemy.dialects import postgresql


def main():
    if 'TIMESCALEDB_CONNECT' not in os.environ:
        sys.stderr.write(
            ("Err.  Please set TIMESCALEDB_CONNECT to your postgresql "
             "connect string\n"))
        sys.exit(2)

    now = datetime.now()
    earliest = now - timedelta(minutes=10)
    q = session.query(Metric).filter(Metric.time > earliest).yield_per(100)
    print(q.statement.compile(dialect=postgresql.dialect()))
    for item in q:
        print(item)
    return 0


if __name__ == "__main__":
    sys.exit(main())
