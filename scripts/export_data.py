#!/usr/bin/env python3

from promchemist.helpers import iter_times
import sys
import pandas as pd
from datetime import datetime
from promchemist.base import session, Metric, Label
import os


def main():

    if 'TIMESCALEDB_CONNECT' not in os.environ:
        sys.stderr.write(
            ("Err.  Please set TIMESCALEDB_CONNECT to your postgresql "
             "connect string\n"))
        sys.exit(2)

    timer_start = datetime.now()
    for start, end in iter_times(
            increment='hours', earliest='yesterday at midnight',
            latest='yesterday at 23:59:59'):

        q = session.query(Metric)\
            .filter(Metric.time.between(start, end))\
            .join(Label)\
            .yield_per(1000)
        df = pd.read_sql(q.statement, q.session.bind)
        df.to_parquet('/tmp/test_out/%s-%s.parquet.gzip' % (start, end),
                      compression='gzip')
        print((datetime.now() - timer_start), start, end)
    return 0


if __name__ == "__main__":
    sys.exit(main())
