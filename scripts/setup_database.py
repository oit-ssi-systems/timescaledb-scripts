#!/usr/bin/env python3
import sys
import hvac
import psycopg2
import logging
import os
import yaml
import socket
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT  # <-- ADD THIS LINE


def main():

    server = socket.getfqdn()

    vault = hvac.Client(url='https://vault-systems.oit.duke.edu')
    if os.path.exists(os.path.expanduser("~/.vault_role.yaml")):
        logging.info("Using AppRrole Auth")
        with open(os.path.expanduser("~/.vault_role.yaml")) as f:
            vault_info = yaml.load(f)
            vault.auth_approle(vault_info['role_id'], vault_info['secret_id'])
    else:
        logging.error("No valid vault auth found 😢")

    creds = vault.read('secret/linux/timescaledb/%s/creds/postgres' %
                       server)['data']
    grafana_creds = vault.read('secret/linux/timescaledb/%s/creds/grafana' %
                               server)['data']
    conn = psycopg2.connect(
        "host=%s user=%s password=%s sslmode=require dbname=postgres" %
        (server, creds['username'], creds['password']))
    conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)  # <-- ADD THIS LINE
    cur = conn.cursor()

    # Set up the right database
    db_name = 'perfdump'
    cur.execute("SELECT datname from pg_database;")
    if db_name not in [r[0] for r in cur.fetchall()]:
        print("Creating Database %s" % db_name)
        cur.execute("CREATE DATABASE %s;" % db_name)
    cur.close()
    conn.close()

    # Now connect to the right database
    conn = psycopg2.connect(
        "host=%s user=%s password=%s sslmode=require dbname=%s" %
        (server, creds['username'], creds['password'], db_name))
    conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)  # <-- ADD THIS LINE
    cur = conn.cursor()

    # Set up grafana
    cur.execute("SELECT rolname FROM pg_roles;")
    if 'grafana' not in [r[0] for r in cur.fetchall()]:
        print("Creating grafana user")
        cur.execute('CREATE ROLE "grafana";')
        cur.execute('ALTER ROLE "grafana" WITH LOGIN;')
        cur.execute('GRANT SELECT ON ALL TABLES IN SCHEMA public TO grafana;')
        cur.execute(
            'ALTER DEFAULT PRIVILEGES IN SCHEMA public GRANT SELECT ON TABLES TO grafana;'
        )
        cur.execute("""ALTER user grafana with password '%s';""" %
                    grafana_creds['password'])

    return 0


if __name__ == "__main__":
    sys.exit(main())
