import dateparser
from datetime import timedelta, datetime


def iter_times(increment='days', latest='now', earliest='7 days ago'):

    earliest_time = dateparser.parse(earliest)
    latest_time = dateparser.parse(latest)

    current_time = earliest_time
    time_ranges = []

    while current_time <= latest_time:
        time = current_time
        if increment == 'days':
            start = datetime(time.year, time.month, time.day, 0, 0)
            end = datetime(time.year, time.month, time.day, 23, 59)
            time_ranges.append([start, end])
            current_time += timedelta(days=1)
        elif increment == 'hours':
            start = datetime(time.year, time.month, time.day, time.hour, 0)
            end = datetime(time.year, time.month, time.day, time.hour, 59, 59)
            time_ranges.append([start, end])
            current_time += timedelta(hours=1)
    return time_ranges
