from sqlalchemy.ext.automap import automap_base
from sqlalchemy.orm import Session
import os
import textwrap
from sqlalchemy import PrimaryKeyConstraint, UniqueConstraint
from sqlalchemy import create_engine, MetaData, Column, ForeignKey
from sqlalchemy import Float, Time, Integer, String
from sqlalchemy.dialects.postgresql import JSONB


Base = automap_base()

# engine, suppose it has two tables 'user' and 'address' set up
engine = create_engine(os.environ['TIMESCALEDB_CONNECT'])
# reflect the tables
metadata = MetaData()
# metadata.reflect(engine, only=['metrics_labels'])
Base = automap_base(metadata=metadata)


class Label(Base):
    __tablename__ = 'metrics_labels'
    __table_args__ = (
        UniqueConstraint('metric_name', 'labels'),
    )

    id = Column('id', Integer, primary_key=True)
    metric_name = Column('metric_name', String)
    labels = Column('labels', JSONB)


class Metric(Base):
    __tablename__ = 'metrics_values'
    __table_args__ = (
        PrimaryKeyConstraint('time', 'labels_id'),
    )

    value = Column('value', Float)
    time = Column('time', Time)
    labels_id = Column('labels_id', Integer, ForeignKey('metrics_labels.id'))

    def __str__(self):
        return(
            "<Metric %s=%s %s>" % (
                self.label.metric_name,
                self.value,
                textwrap.shorten("%s" % self.label.labels, width=75,
                                 placeholder="...")
            )
        )


Base.prepare()
session = Session(engine)
