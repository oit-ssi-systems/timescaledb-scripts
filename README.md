# Using this repo

Set up pwd for the PYTHONPATH

```
export PYTHONPATH=$(pwd)
```

Export an env variable for the SQL connection

```
export TIMESCALEDB_CONNECT="postgres://postgres:PASS@SERVER:5432/DB?sslmode=require"
```

When doing custom sqlalhemy bits, be sure to use `yield_per`, since we have so
much data in this thing

# Useful Postgreql queries

Select Noisy metrics
```
SELECT name, count(value)
FROM metrics
  WHERE time > NOW() - interval '10 minute'
GROUP BY name
ORDER BY count DESC;
```

Show running processes
```
SELECT * from pg_stat_activity ;
```
